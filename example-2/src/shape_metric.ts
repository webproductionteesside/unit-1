export interface ShapeMetric {
  area(): number;
  perimeter(): number;
}
