import { Square } from "./square"
import { ShapeMetric } from "./shape_metric"

const mySquare = new Square(10, 10, 10);

const myTriangle = {
  baseLen: 5,
  height: 10
};

console.log("mySquare area: ", mySquare.area());

function displayArea(obj: ShapeMetric) {
  console.log("The area of the unknown shape is: ", obj.area());
}

//Will work because Square implements ShapeMetric
displayArea(mySquare);

//Wont work becuase Object does not implement ShapeMetric
//
//tsc error:
//  error TS2345: Argument of type '{ baseLen: number; height: number; }' is not assignable to parameter of type 'ShapeMetric'.
//  Property 'area' is missing in type '{ baseLen: number; height: number; }'.

//displayArea(myTriangle);
