import { Shape, ShapeMetric } from "./shape";

export class Square extends Shape {
  constructor(private sideLen_ : number, x : number, y : number) {
    super(x, y);
  }

  get sideLen(): number {
    return this.sideLen_;
  }

  //Implement ShapeMetric:
  public area(): number {
    return this.sideLen_ * this.sideLen_;
  }

  public perimeter(): number {
    return this.sideLen_ * 4;
  }
}
