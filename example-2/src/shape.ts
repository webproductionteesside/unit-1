import { ShapeMetric } from "./shape_metric";
export { ShapeMetric };

export abstract class Shape implements ShapeMetric {
  constructor(private x_ : number, private y_ : number) {

  }

  get x() : number {
    return this.x_;
  }

  get y() : number {
    return this.y_;
  }

  abstract area(): number;

  abstract perimeter(): number;

}
