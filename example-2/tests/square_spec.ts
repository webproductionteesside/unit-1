import { expect } from 'chai';
import 'mocha';
import { Square } from '../src/square';



describe(
  'TestClass Initial Test',
  () => {
    it(
      'should return an area of 100',
      () => {
              const testSquare = new Square(10, 1, 10);

              expect(testSquare.area()).equal(100);
      });
});
