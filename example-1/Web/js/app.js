/***
 * Note: This is written in JavaScript not TypeScript
 *
 *       ** You DO NOT need to edit this file **
 */
require.config({
  "baseUrl": ".",
  "paths": {
    "counter": "../build/counter",
    "jquery": "https://code.jquery.com/jquery-2.2.4.min"
  }
});

require(["./js/main"])
