/***
 * Note: This is written in JavaScript not TypeScript
 *
 *       ** You SHOULD edit this file as JavaScript to experiment with Counter class **
 */
define(
  ["jquery", "counter"],
  ($, counter) => {
    //$: for usual jQuery usage
    //counter: is a module where the Counter class is found, e.g: new counter.Counter(10)

    // Your experiment code goes here!
  }
);
