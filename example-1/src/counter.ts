export class Counter {
  private value_: number = 0;

  constructor(private readonly maximum_: number) {

  }

  public increment(): number {
    if(!this.atMaximum()) {
      ++this.value_;
    }
    return this.value_;
  }

  public decrement(): number {
    if(this.value_ != 0) {
      --this.value_;
    }
    return this.value_;
  }

  public reset() { this.value_ = 0; }

  public atMaximum(): boolean { return this.value_ == this.maximum_; }

  public get value(): number { return this.value_; }

  public get maximum(): number { return this.maximum_; }
}
