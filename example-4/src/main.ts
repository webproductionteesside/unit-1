import { TestClass } from "./test_class";

const tc1 = new TestClass();
TestClass.value = 10;
console.log(tc1.value); // outputs: 10​

const tc2 = new TestClass();​
TestClass.value = 32;
console.log(tc2.value); // outputs: 32​

console.log(tc1.value + tc2.value); // outputs: 64​
