export class TestClass {​

  private static value_: number = 0;

  constructor() {​

  }​

  // Class methods
  //
  static set value(v: number) { this.value_ = v; }
  static get value(): number { return this.value_; }​

  // Instance variable methods:
  // (These are redundant)
  set value(v: number) { TestClass.value_ = v; }
  get value(): number { return TestClass.value_; }

}
