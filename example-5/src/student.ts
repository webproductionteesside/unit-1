import { Counter } from "./counter";

export class Student {​

  private static studentCounter: Counter = new Counter(100000);
  private idNumber_: number;

  constructor(private name_: string) {​
    this.idNumber_ = Student.studentCounter.increment();
  }​

  get idNumber(): number { return this.idNumber_; }​
  get name(): string { return this.name_; }

}
