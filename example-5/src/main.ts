import { Student } from "./student";

const STUDENT_NAMES: Array<string> = [
  "Joe Bloggs", "Mary Quitecontrary", "Jon Doe",
  "Barry Hebbron", "Arnold Schwarzenegger", "Dame Judy Dench", "Maggie Q"
];

for(let student_name of STUDENT_NAMES) {
  let student = new Student(student_name);
  console.log(student.name + " " + student.idNumber);
}
